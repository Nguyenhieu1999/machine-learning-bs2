import numpy as np
from numpy import random as rd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import pandas as pd

dataset=pd.read_csv('Productdata.csv')
dataset.shape
dataset.head()

X = dataset.iloc[:, [1, 2]].values

m=X.shape[0] #number of training examples
n=X.shape[1] #number of features. Here n=2
n_iter=100
#Cách 1: dùng Euclid
K=5 # number of clusters
Centroids=np.array([]).reshape(n,0)

for i in range(K):
    rand=rd.randint(0,m-1)
    Centroids=np.c_[Centroids,X[rand]]
    Output={}

EuclidianDistance=np.array([]).reshape(m,0)
for k in range(K):
        tempDist=np.sum((X-Centroids[:,k])**2,axis=1)
        EuclidianDistance=np.c_[EuclidianDistance,tempDist]
C=np.argmin(EuclidianDistance,axis=1)+1

Y={}
for k in range(K):
    Y[k+1]=np.array([]).reshape(2,0)
for i in range(m):
    Y[C[i]]=np.c_[Y[C[i]],X[i]]
     
for k in range(K):
    Y[k+1]=Y[k+1].T
    
for k in range(K):
     Centroids[:,k]=np.mean(Y[k+1],axis=0)

for i in range(n_iter):

      EuclidianDistance=np.array([]).reshape(m,0)
      for k in range(K):
          tempDist=np.sum((X-Centroids[:,k])**2,axis=1)
          EuclidianDistance=np.c_[EuclidianDistance,tempDist]
      C=np.argmin(EuclidianDistance,axis=1)+1

      Y={}
      for k in range(K):
          Y[k+1]=np.array([]).reshape(2,0)
      for i in range(m):
          Y[C[i]]=np.c_[Y[C[i]],X[i]]
     
      for k in range(K):
          Y[k+1]=Y[k+1].T
    
      for k in range(K):
          Centroids[:,k]=np.mean(Y[k+1],axis=0)
      Output=Y
      
plt.scatter(X[:,0],X[:,1],c='black',label='unclustered data')
plt.xlabel('Sellnumber')
plt.ylabel('Price (k$)')
plt.legend()
plt.title('Data chưa cluster')
plt.show()

color=['red','blue','green','cyan','magenta']
labels=['Nhóm 1','Nhóm 2','Nhóm 3','Nhóm 4','Nhóm 5']
for k in range(K):
    plt.scatter(Output[k+1][:,0],Output[k+1][:,1],c=color[k],label=labels[k])
plt.scatter(Centroids[0,:],Centroids[1,:],s=300,c='orange',label='Centroids')
plt.xlabel('Sellnumber')
plt.ylabel('Price (k$)')
plt.title('Phân chia nhóm theo giá và số lượng bán (Dùng khoảng cách Euclid)')
plt.legend()
plt.show()

#Cách 2: Dùng Kmeans     
# Lắp K-Means vào bộ dữ liệu
kmeans = KMeans(n_clusters = 5, init = 'k-means++', random_state = 100)
y_kmeans = kmeans.fit_predict(X)
# Visualising the clusters
plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s = 100, c = 'red', label = 'Nhóm 1')
plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Nhóm 2')
plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Nhóm 3')
plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Nhóm 4')
plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'magenta', label = 'Nhóm 5')
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 300, c = 'yellow', label = 'Center')
plt.title('Dùng Kmeans')
plt.xlabel('Sellnumber')
plt.ylabel('Price (k$)')
plt.legend()
plt.show()